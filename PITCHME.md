## Formació en seguretat i privacitat

Valors, eines i conceptes per protegir la privacitat i
la seguretat de la informació.

#VSLIDE

* Autoria
  * 2020 Hacklab Logout (mDescape, fadelkon, +)
  * 2018 fadelkon
  * 2017 críptica (charlieMKR i fadelkon)
* Llicència: [CC - Reconeixement i Compartir igual](https://creativecommons.org/licenses/by-sa/4.0/deed.ca)

#HSLIDE

## Criteris per triar eines de comunicació

#VSLIDE

## Topologia de xarxa
* __Peer to Peer o Friend to Friend:__ No calen servidors, els ordinadors es connecten entre ells (Retroshare, Ring, Tox, Ricochet).
* __Descentralitzada:__ La comunicació depèn de servidors, que es coordinen entre ells. (e-Mail, Matrix, Jabber, Quitter, Diaspora).
* __Centralitzada:__ La comunicació depèn de servidors i són gestionats per una sola entitat (Signal, Whatsapp, Facebook).

#VSLIDE

## Més tipus de (des)centralització
* __d'arquitectura__: Quantes màquines hi ha i a quants llocs són?
* __política__: Qui pren les decisions? Quants òrgans?
* __lògica__: Des de fora, funciona com un bloc uniforme o com un eixam divers?

Vitalik Buterin: [The Meaning of Decentralization](https://medium.com/@VitalikButerin/the-meaning-of-decentralization-a0c92b76a274)

#VSLIDE

## Informació en moviment

* __Extrem a extrem:__ Les dades es xifren a la màquina de l'emisor i es desxifren a la del receptor (Signal).
* __Punt a punt:__ Les dades es xifren per viatjar d'una màquina a una altra pero es desxifren en punts intermedis (Webs, Telegram, e-mail).
* __En clar:__ Les dades no es xifren en cap moment i viatgen "tal qual" (DNS, Whatsapp pre-2012, webs sense TLS).

#VSLIDE

## Informació en respòs
* Discos durs xifrats quan no estan en ús (LUKS, Bitlocker, VeraCrypt)
* Informació xifrada amb la contrasenya d'usuària (Riseup, Posteo, Protonmail, Tutanota)
* Màquines no externalitzades. Control físic de la infraestructura.

#VSLIDE

## Privacitat per disseny vs. per política
* __Per disseny:__ El propi disseny del programa fa que les dades no es puguin accedir pel proveidor (Tor).
* __Per política:__ Les dades són accessibles pel proveïdor, però la seva política és no fer-ho, o esborrar-les (OpenMailbox, Riseup, Posteo...).
* __Cap:__ «Recollim algunes dades per millorar el servei i les compartim amb empreses associades»

#VSLIDE

## Llicència del codi
* __Lliure:__ El codi font del programa es públic, i revisable. Es pot modificar i distribuir versions alternatives (Firefox, Revolt).
* __Obert__: El codi font és disponible però no permet ser reutilitzat. O bé, "lliure" però blanquejat de valors altruistes.
* __Privatiu:__ El codi font no és accessible i per tant no es pot revisar, no es pot saber del cert què fa, ni els errors que té (Google Chrome, Whatsapp).

#VSLIDE

## Abast de la protecció
* __Dades i la majoria de metadades:__ Tant el contingut dels missatges com la info de suport estan protegides. Es pot deduir molt poc (Ricochet).
* __Dades:__ El contingut dels missatges està protegit, però certa informació de suport queda a la vista: emissor, receptor, graf social, horaris... (e-mail xifrat amb GPG).
* __Cap:__ El contingut dels missatges i tota la informació de suport és accessible per entitats diferents a l'emissora i al receptora (Telegram i e-mail sense xifrar).

#VSLIDE

## Taula resum 1/3

Concepte | Desitjat | Subòptim   | Inacceptable
---------|----------|------------|-------------
Topologia de xarxa | P2P. F2F|Federada o centralitzada| "Gàbia d'or"
Xifrat | Extrem a extrem | Punt a punt | Cap xifrat 
Informació protegida | Dades i metadades | Només dades | Cap protecció 

#VSLIDE

## Taula resum 2/3

Concepte | Desitjat | Subòptim   | Inacceptable
---------|----------|------------|-------------
Model de desenvolupament | Comunitari | Individual. Empresarial | -
Retribució | Cooperatiu-social | Voluntaris. Ànim de lucre "net" | Ànim de lucre "brut".
Llicència | Codi lliure| Codi obert | Codi privatiu

#VSLIDE

## Taula resum 3/3

Concepte | Desitjat | Subòptim   | Inacceptable
---------|----------|------------|-------------
Opcions de privacitat | Per defecte | Opcionals | Inexistents 
Protecció | Per disseny del sistema | Per política d'empresa | Inexistent 

#VSLIDE

## Altres aspectes a avaluar
* Impacte ecològic
* Inclusivitat, accesibilitat
* Impacte social
* Comunitat i documentació

  #HSLIDE

### Missatgeria instantània: Signal
![Signal](signal/img/signal-logo.png)

#VSLIDE

#### Signal és
Un sistema de missatgeria instantània semblant a Whatsapp i a Telegram. Proporciona seguretat **extrem a extrem** i està gestionada per una associació sense ànim de lucre.

#VSLIDE

### PROS:

* Autodestrucció de missatges opcional
* Privacitat per disseny amb les dades.
* Xifrat extrem-a-extrem per text i multimèdia.
* Altament usable, semblant a Whatsapp.
* Codi i protocol de xifrat obert.
* Privacitat per defecte.
* Bloqueja els missatges si les claus de xifrat han canviat i les claus es poden comprobar fàcilment.

#VSLIDE

### CONTRES:

* Registre amb número de telèfon mòbil: no anònim, altament regulat
* Sistema 100% centralitzat.
* Les metadades no estan protegides per disseny, pero sí per política.

#VSLIDE
![Signal per Android](signal/img/signal-android.jpg)

#VSLIDE

![Captura de pantalla de Signal demostrant la funcionalitat d'esborrar cares amb una foto de manifestants](signal/img/signal_blur.png)

#VSLIDE

##### Petició de Dades per part del FBI, publicada per OWS i ACLU (Octubre 2016)
![La petició del FBI](signal/img/subpoena-fbi-signal.png)
* OWS: Open Whisper Systems. Associació que desenvolupa i manté Signal.
* ACLU: American Civil Liberties Union. Organització a favor dels drets civils dels USA.

#VSLIDE

#### La informació proporcionada per OWS
![La informació que tenia OWS](signal/img/signal-subpoena-reply.png)

[Més informació](http://arstechnica.com/tech-policy/2016/10/fbi-demands-signal-user-data-but-theres-not-much-to-hand-over/) (en anglès)

#HSLIDE

### Anonimat a la xarxa: Tor
![Logo de Tor](tor/img/tor-logo.png)

#VSLIDE

#### Tor és
* Una **xarxa** d'ordinadors sobre internet per anonimitzar.
* El **programa** que connecta els ordinadors a la xarxa Tor.
* El **navegador** que funciona a través de la xarxa Tor.

#VSLIDE

#### Funcions
* Amaga l'adreça IP
* Xifra el trànsit
* Serveis onion

#VSLIDE

#### Contrapartides
* Retard i ample de banda limitat
* Crida l'atenció
* Bloquejos, traves

#VSLIDE

#### Adreça IP

**Destí** i **remitent** d'una comunicació a internet.

El nostre remitent és una **adreça pública**.

Pot ser la de:
* casa
* biblioteca
* local social
* …

#VSLIDE

#### Adreça IP

Les ISP tenen comprats **rangs** d'adreces IP, i estan repartits per **zones** geogràfiques. Per tant, la nostra adreça IP pública revela:
* La companyia telefònica contractada
* La localització física aproximada

#VSLIDE

#### Adreça IP
Podem esbrinar la informació que escampem per internet amb serveis com [ipleak.net](https://ipleak.net)
![Adreça IP](tor/img/ip-leak_clearnet.gif)

#VSLIDE

#### Internet ≠ WWW
A Internet hi funcionen molts protocols d'aplicació. Per conviure en la mateixa màquina, es fan servir ports TCP o UDP.

La Web fa servir els següents ports
* HTTP: TCP-80
* HTTPS: TCP-443
* Resolució de noms (DNS): UDP-53

#VSLIDE

#### Internet ≠ WWW
Però n'hi ha molts més, de protocols
* Xat (Jabber): TCP-5222
* Correu: TCP-993, TCP-587, TCP-465
* Age of Empires II: TCP-47624

#VSLIDE

#### Internet ≠ WWW
Actualment la tendència és definir nous protocols per sobre de HTTP: les **API web**
* API de Twitter
* Xat (Matrix.org, RocketChat)
* Xarxes socials lliures (federació entre servidors)

#VSLIDE

#### Connexió HTTP, sense xifrar
![Connexió HTTP](tor/img/internet_16-9.png)

#VSLIDE

#### Connexió xifrada, TLS.
Xifra la connexió entre client i servidor.
* Web, mail, xat, …

Els túnels TLS ens protegeixen d'atacs:
* **Passius**: robar credencials, llegir formularis i missatges
* **Actius**: manipular webs, programes, actualitzacions.

#VSLIDE

#### Connexió HTTPS, xifrada

![Connexió HTTPS](tor/img/tls-internet_16-9.png)

#VSLIDE

#### Adreça IP del remitent **sense** usar **Tor**
Des de Barcelona i amb Movistar:
![Adreça IP sense Tor](tor/img/ip-leak_clearnet.gif)

#VSLIDE

#### Adreça IP del remitent **usant Tor**
Des de Barcelona i amb Movistar:
![Adreça IP amb Tor](tor/img/ip-leak_tor.gif)

#VSLIDE

#### Connexió a través de Tor
![Connexió amb Tor](tor/img/tor-internet_16-9.png)

#VSLIDE

#### Nodes de sortida a Clearnet

* Pocs: ample de banda limitat.
* Perillosos: Imprescindible HTTPS.

#VSLIDE

#### Connexió a través de Tor
![Connexió amb Tor i TLS](tor/img/tor-tls-internet_16-9.png)

#VSLIDE

#### Serveis onion

Comunicació entre desconeguts
* Servidor anònim.
* Usuària anònima.

En comparació a serveis a la clearnet:
* No congestiona els nodes de sortida.
* No necessita HTTPS.

#VSLIDE

#### Alguns llocs web onion

* Hidden Wiki: http://zqktlwi4fecvo6ri.onion/wiki/
* Críptica: http://cripticavraowaqb.onion
* Wikileaks: http://wlupld3ptjvsgwqw.onion/
* Webmail: http://sigaintevyh2rzvw.onion/

#VSLIDE

#### Aplicacions basades en serveis onion
* OnionShare: compartir fitxers
* Ricochet: xat auster

#VSLIDE

#### Aplicacions torificables:
* Navegador: Firefox → Tor Browser Bundle
* Correu: Thunderbird → extensió TorBirdy
* App Twitter oficial → app Twidere + Orbot

#HSLIDE

### Anonimat multimèdia: ObscuraCam
![Obscuracam Banner](obscuracam/img/obscuracam_banner.png)


#VSLIDE
#### Funcions
* Amagar cares o objectes identificatius
* Esborrar les metadades EXIF de fotos i vídeos

#VSLIDE
#### Pixela cares
* Detecta automàticament les cares de la foto
* Permet afegir i modificar les censures

![Cares pixelades](obscuracam/img/cares_pixelades.png)

#VSLIDE
#### Metadades EXIF
Fotografia adorable...

![Nens menjant gelat](obscuracam/img/nens_gelat.jpg)

#VSLIDE
#### Metadades EXIF
... amb més informació del que sembla!
```
user@host:~$ exiftool nens_gelat.jpg 
File Type         : JPEG
MIME Type         : image/jpeg
Camera Model Name : SM-G900V
ISO               : 64
Exif Version      : 0220
Flash             : No Flash
Aperture          : 2.2
GPS Altitude      : 18 m Below Sea Level
GPS Date/Time     : 2016:04:17 20:25:10Z
GPS Latitude      : 40 deg 42' 56.07" N
GPS Longitude     : 73 deg 50' 36.35" W
GPS Position      : 40 deg 42' 56.07" N, 73 deg 50' 36.35" W
Shutter Speed     : 1/120
Create Date       : 2016:04:17 16:25:24.890
Focal Length      : 4.8 mm (35 mm equivalent: 31.0 mm)
```

#VSLIDE
#### Metadades EXIF: model de càmera
```
Camera Model Name : SM-G900V
```
![EXIF: Model de la càmera](obscuracam/img/exif_camera-model.png)

#VSLIDE
#### Metadades EXIF: ubicació
```
GPS Position      : 40 deg 42' 56.07" N, 73 deg 50' 36.35" W
```
![EXIF: Ubicació](obscuracam/img/exif_gps.png)

#VSLIDE
#### Resultat metadades
Després de passar la foto per Obscuracam,
veiem que tota la informació compromesa ha estat esborrada.
```
user@host:~$ exiftool nens_gelat_neta.jpg 
File Type         : JPEG
MIME Type         : image/jpeg
```
#VSLIDE
#### Resultat cares
_«Alguien ha pensado en los niños»_

![EXIF: pixelades](obscuracam/img/nens_gelat_neta.jpg)

#VSLIDE
#### Com instaŀlar i fer servir Obscuracam
* Security in a Box: [Android: Obscuracam](https://securityinabox.org/es/obscuracam)
* FLOSS Manuals: [Obscuring Photos](http://booki.flossmanuals.net/obscuracam/obscuring-photos)

#VSLIDE
#### Metadades: més info
* Què són les metadades: [wiki de "Metadata Anonymisation Toolkit"](https://mat.boum.org/)
* Eina exiftool: https://linux.die.net/man/1/exiftool

#VSLIDE
#### Reconeixement
* Nens adorables: [Joe Shlabotnik "Kids Eating Ice Cream On The School Steps" - CC by-nc-sa v2.0](https://www.flickr.com/photos/joeshlabotnik/31892072800/)
* Cares pixelades: [FLOSS Manuals Foundation - CC by-sa v3.0](https://flossmanuals.net)

#HSLIDE

### Complements per Firefox

![Logo firefox](firefox-addons/img/firefox.png)

#VSLIDE
#### Necessitat
La navegació per internet no és privada ni segura per defecte:
* Pàgines sense HTTPS per defecte
* Anuncis basats en els teus hàbits de navegació
* Cookies amb dates de caducitat molt llunyanes
* Programes que monitoren la nostra activitat a les pàgines que visitem

#VSLIDE
#### HTTPS Everywhere
![Logo HTTPS Everywhere](firefox-addons/img/https-everywhere.png)

* Prova la comunicació xifrada (HTTPS) si el servidor la suporta.
* Té un mode de bloquejar totes les connexions per HTTP sense xifrar.
* [Descarrega'l](https://addons.mozilla.org/ca/firefox/addon/https-everywhere/)

#VSLIDE
#### uBlock Origin
![Logo uBlock Origin](firefox-addons/img/ublock-origin.png)

* Bloqueja els anuncis de manera eficient i permet crear els teus propis filtres
* [Descarrega'l](https://addons.mozilla.org/ca/firefox/addon/ublock-origin/)

#VSLIDE
#### Cookie-AutoDelete
![Logo cookie-auto-delete](firefox-addons/img/cookie-auto-delete.png)
* Destrueix les cookies de qualsevol pestanya que no tinguis oberta
* [Descarrega'l](https://addons.mozilla.org/ca/firefox/addon/cookie-autodelete/)

#VSLIDE
#### Privacy badger
![Logo Privacy Badger](firefox-addons/img/privacy-badger.png)

* Bloqueja els continguts de tercers que tinguin l'objectiu de rastrejar
* [Descarrega'l](https://addons.mozilla.org/en-US/firefox/addon/privacy-badger17/)

#HSLIDE

### Gestió de credencials: KeepassX
![KeepassX logo](keepassx/img/keepassx_logo.png)

#VSLIDE
#### Els 3 manaments de les contrasenyes
* _No les reutilitzaràs_
* _No les compartiràs_
* _No les faràs febles_

#VSLIDE
#### Propietats
* Caràcters diferents
* **Llargues**
* Difícils d'endevinar
    * Patrons evidents no
    * Dades conegudes no: noms, dates, aficions, etc.

#VSLIDE
#### Gestió ideal de les contrasenyes
* Una per cada compte → moltes!
* Difícils d'endevinar → aleatòries!
* Difícils de trencar a "força bruta" → llargues!

#VSLIDE
#### Exemples
Exemples de menys a més fortes:
* Indigna: `tkmamor`
* Txepi-txepi: `tkmuchoMIam0r!<3`
* Idestructible:
  `mi corazon palpita como una patata frita tutum tutum`

#VSLIDE
#### Les matemàtiques darrere
Millor invertir en longitud que en caràcters diferents.

* **x**: Possibilitats per cada caràcter (només minúscules: 26)
* **y**: Longitud en caràcters
* **z**: Nombre total de possibles contrasenyes

  ![z = x^y](keepassx/img/possibilitats.png)
* **H**: Entropia (complexitat). Objectiu: més de 100 bits

   ![H = log2(z) = y*log2(x)](keepassx/img/entropia.png)

#VSLIDE
#### Representació en 3D
![gràfic longitud vs símbols](keepassx/img/entropia3d.png)

#VSLIDE
#### Gestió habitual de les contrasenyes
* Una per TOTS els comptes
* Fàcil d'endevinar → aniversaris, noms propers, …
* Fàcil de trencar → pocs caràcters

**TENIM UN PROBLEMA**

#VSLIDE
#### Gestor de contrasenyes
* Base de dades xifrada
* Una contrasenya per dominar-les a totes
* Generador de contrasenyes fortes

#VSLIDE
#### Com fer-la servir
* Organització
    * per grups, subgrups i entrades
    * entrades desordenades i buscador
* Fer còpies de seguretat

#VSLIDE
* Tutorial a Código Sur (2020): [KeePassXC: Almacenar y generar contraseñas de manera segura](https://blog.codigosur.org/keepassxc-almacenar-y-generar-contrasenas-de-manera-segura/)
* Tutorial a Surveillance Self Defense: [Cómo usar KeepassX](https://ssd.eff.org/es/module/c%C3%B3mo-usar-keepassx)
* Tutorial a Security in a Box: [KeePass – Almacenamiento seguro de contraseñas](https://securityinabox.org/es/keepass_main)
* Calculadora de robustesa de contrasenyes: [passfault](https://passfault-hrd.appspot.com/)

#HSLIDE

### El meu veí Google

![pancarta de fuckoffgoogle a berlin](cercadors/img/google.jpg)

#VSLIDE
#### El monstre de Google
Google ens rastreja i ens estudia com ratolins maŀleables.
* Amb l'historial de cerca (Google)
* Amb l'historial de navegació web (Chrome)
* Amb els anuncis en altres llocs (AdSites)
* Amb anàlisi per admins de webs (Analytics)
* Amb el mòbil (Ubicació, Aplicacions, Contactes, Mail)
* … tot?

Més informació a [GAFAM si pots!](https://gafam-si-pots.hotglue.me/google/)

#VSLIDE
#### Cercadors

* NO **Google**, trafica amb dades, és un règim dictatorial digital
* NO **Bing**, Microsoft trafica amb dades, és un règim dictatorial digital
* NO **Yandex**, trafica amb dades, és un règim dictatorial digital
* NO **Ecosia**, planta arbres a canvi de vendre la teva identitat als mercats de tràfic de dades, màrketing i manipulació social

#VSLIDE

#### Cercadors
* SÍ [Duckduckgo](https://duckduckgo.com/) Cercador. USA. Anuncis no personalitzats.
* SÍ [StartPage/Ixquick](https://www.startpage.com/)  Metacercador. UE. Anuncis no personalitzats.
* SÍ [Qwant](http://qwant.com/) Cercador. FR. Anuncis no personalitzats.
* SÍ Searx. Metacercador. Lliure.
   * [searx.me](https://searx.me)
   * [search.fuckoffgoogle.net](https://search.fuckoffgoogle.net/)
   * [searx.laquadrature.net/](https://searx.laquadrature.net/)

#VSLIDE
#### Correu

* Coŀlectius activistes, per donacions
    * [es] [SinDominio](https://sindominio.net)
    * [it] [Autistici/Inventati](https://autistici.org)
    * [us] [RiseUp](https://riseup.net)
    * [us] [Aktivix](https://aktivix.org/)

#VSLIDE
#### Correu

* Cooperatives, associacions i empreses properes
    * [de] [Posteo](https://posteo.de/)
    * [cat] [Pangea](https://pangea.org)
    * [cat] [La mar de bits](https://lamardebits.org)
    * [cat] [Maadix](https://maadix.net)

#VSLIDE
#### Correu

Llistes més completes:
* [Riseup: Radical servers](https://riseup.net/en/security/resources/radical-servers)
* [Privacy Tools](https://www.privacytools.io/providers/email/)
* [Privacy-Conscious Email Services](https://www.prxbx.com/email/)

#VSLIDE

#### "El Drive"

* Google ho indexa tot
* Censura indiscriminadament:
  * treballadores sexuals
  * dissidents polítics
  * "errors"
* Ens crea més dependència a internet

#VSLIDE
#### Arxius: Solucions offline
* Pendrives (sempre tenen bateria i cobertura)

![pen en forma de nina russa](cercadors/img/pendrive.jpg)

#VSLIDE
#### Arxius: Solucions online
* Serveis d'enviament caduc
  * [send.firefox.com](https://send.firefox.com)
  * [share.riseup.net](https://share.riseup.net)
  * [framadrop.org](https://framadrop.org/)

#VSLIDE
#### Arxius: Solucions online
*  Nextcloud
  * [Proveïdores comercials](https://nextcloud.com/providers/)
  * [Maadix](https://maadix.net/)
  * [Pangea](https://pangea.org/)
  * [Disroot](https://disroot.org/es/services/nextcloud)
  * [Indie hosters](https://indie.host/)

#VSLIDE
#### Documents: solucions offline

LibreOffice (encara funciona! I millor que abans!)

![els logos de les 5 aplicacions de libreoffice](cercadors/img/libreoffice.jpg)

#VSLIDE
#### Documents: solucions online
* [ProtectedText](https://www.protectedtext.com/): xifrat amb una contrasenya i edició separada
* [CryptPad](https://cryptpad.fr/): xifrat però públic amb enllaç
* [Nextcloud Text](https://apps.nextcloud.com/apps/text): coŀlaboració en temps integrada
* CodiMD [demo gratis](https://demo.codimd.org) o [registre](https://hackmd.io/)
* Nextcloud + [Collabora Online](https://nextcloud.com/collaboraonline/): edita ODT online
* Nextcloud + [OnlyOffice](https://nextcloud.com/onlyoffice/): edita DOCX online
* [JetPad](https://jetpad.net/): amb formatat i coŀlaboració instantània
* EtherPad. Els clàssics, fàcils, públics:

#VSLIDE
#### Documents: solucions online
* EtherPad
   * [Riseup](https://pad.riseup.net/)
   * [Codigosur](https://pad.codigosur.org/)
   * [Vedetas](https://antonieta.vedetas.org)
   * [Framasoft](https://mensuel.framapad.org/)
   * [Kefir](https://pad.kefir.red/)
   * [Disroot](https://pad.disroot.org/)

#VSLIDE
#### Mapes: OpenStreetMaps
La wikipèdia dels mapes
* [Estàndard amb 3 capes](https://www.openstreetmap.org/way/157016134)
* [Opentopomap](https://opentopomap.org/#map=17/41.44536/2.15773): corbes de nivell i muntanya
* Umap: crea un mapa personalitzat
   * [FramaSoft](https://framacarte.org)
   * [OSM-FR](http://umap.openstreetmap.fr)

#VSLIDE
#### Mapes: OpenStreetMaps
* Per a mòbil
  * [OsmAnd](https://fossdroid.com/a/osmand~.html) Complet, més configurable
  * ["Maps"](https://fossdroid.com/a/maps.html) Complet, més ràpid

#VSLIDE
#### Mapes: Institut Cartogràfic i Geològic de Catalunya

* [Instamaps](https://www.instamaps.cat/): crea un mapa personalitzat
* [Mapa polític/topogràfic i de satèl·lit](http://www.icc.cat/vissir3/index.html?ryKiLHg78)
* [Altres](http://betaportal.icgc.cat/)

#VSLIDE
#### Mapes: Rutes/navegació
* Indicacions
   * [Mou-te](https://mou-te.gencat.cat): Amb transport públic, a catalunya
   * [OpenStreetMaps](https://www.openstreetmap.org/directions?engine=graphhopper\_bicycle\&route=41.3464%2C1.6995%3B41.5113%2C1.7016#map=11/41.4286/1.7104): A peu, bici o cotxe
   * [GrassHopper](https://graphhopper.com/maps/) Vehicles privats, ruta multipunt
* Navegació
  * [OsmAnd](https://fossdroid.com/a/osmand~.html) Complet, més configurable
  * ["Maps"](https://fossdroid.com/a/maps.html) Complet, més ràpid

#VSLIDE
### Ànims! :D
_No ho farem ni tot sols, ni tot de cop_

![imatge de complicitat de dos joves de dibuixos animats amb un smartphone](cercadors/img/nothing-to-hide.jpg)

_Sinó pas a pas i agafades de la mà_

#HSLIDE

### Gràcies per l'atenció. Estem en contacte!
* **Mail** fadelkon(arroba)posteo.net
* **Clau PGP** [5B0B424A24EB0747084D49EB01A919E37F36BE1B](https://keys.openpgp.org/vks/v1/by-email/fadelkon%40posteo.net)
* **Fedivers** [fadelkon@anem.prou.be](https://anem.prou.be/fadelkon)
* **Blog compartit** [sub.marin.li](https://sub.marin.li)
